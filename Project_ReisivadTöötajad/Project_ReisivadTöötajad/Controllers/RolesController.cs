﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_ReisivadTöötajad.Models;

namespace Project_ReisivadTöötajad.Controllers
{
    public class RolesController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        // GET: Roles
        public ActionResult Index()
        {
            var loginUser = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();  //sisseloginu andmed

            if (loginUser == null || loginUser.PersonState == 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Access Denied");
            }

            return View(db.Roles.OrderBy(id => id.Id).ToList());
        }

        // GET: Roles/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Role role = db.Roles.Find(id);
            if (role == null)
            {
                return HttpNotFound();
            }

            /*ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Roles;Details;Get;" + ";Role Id;" + role.Id + ";Role Name;" + role.RoleName });
            db.SaveChanges();*/ //võtsin praegu välja

            return View(role);
        }

        // GET: Roles/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Roles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,RoleName")] Role role)
        {
            if (ModelState.IsValid)
            {
                db.Roles.Add(role);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Roles;Create;Post;" + ";Role Id;" + role.Id + ";Role Name;" + role.RoleName });
            db.SaveChanges();

            return View(role);
        }

        // GET: Roles/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Role role = db.Roles.Find(id);
            if (role == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Roles;Edit;Get;" + ";Role Id;" + role.Id + ";Role Name;" + role.RoleName });
            db.SaveChanges();

            return View(role);
        }

        // POST: Roles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,RoleName")] Role role)
        {
            if (ModelState.IsValid)
            {
                db.Entry(role).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Roles;Edit;Post;" + ";Role Id;" + role.Id + ";Role Name;" + role.RoleName });
            db.SaveChanges();

            return View(role);
        }

        // GET: Roles/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Role role = db.Roles.Find(id);
            if (role == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Roles;Delete;Get;" + ";Role Id;" + role.Id + ";Role Name;" + role.RoleName });
            db.SaveChanges();

            return View(role);
        }

        // POST: Roles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Role role = db.Roles.Find(id);
            db.Roles.Remove(role);
            db.SaveChanges();

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Roles;Delete;Post;" + ";Role Id;" + role.Id + ";Role Name;" + role.RoleName });
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
