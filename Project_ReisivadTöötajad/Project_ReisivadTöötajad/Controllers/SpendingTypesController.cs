﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_ReisivadTöötajad.Models;

namespace Project_ReisivadTöötajad.Controllers
{
    public class SpendingTypesController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        // GET: SpendingTypes
        public ActionResult Index()
        {
            var loginUser = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();  //sisseloginu andmed

            if (loginUser == null || loginUser.PersonState == 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Access Denied");
            }

            return View(db.SpendingTypes.ToList());
        }

        // GET: SpendingTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpendingType spendingType = db.SpendingTypes.Find(id);
            if (spendingType == null)
            {
                return HttpNotFound();
            }

           /* ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "SpendingTypes;Details;Get;" + ";Spending Type Id;" + spendingType.Id + ";Spending Name;" + spendingType.Id });
            db.SaveChanges();*/ //võtan praegu välja

            return View(spendingType);
        }

        // GET: SpendingTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SpendingTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SpendingName")] SpendingType spendingType)
        {
            if (ModelState.IsValid)
            {
                db.SpendingTypes.Add(spendingType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "SpendingTypes;Create;Post;" + ";Spending Type Id;" + spendingType.Id + ";Spending Name;" + spendingType.Id });
            db.SaveChanges();

            return View(spendingType);
        }

        // GET: SpendingTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpendingType spendingType = db.SpendingTypes.Find(id);
            if (spendingType == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "SpendingTypes;Edit;Get;" + ";Spending Type Id;" + spendingType.Id + ";Spending Name;" + spendingType.Id });
            db.SaveChanges();

            return View(spendingType);
        }

        // POST: SpendingTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SpendingName")] SpendingType spendingType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(spendingType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "SpendingTypes;Edit;Post;" + ";Spending Type Id;" + spendingType.Id + ";Spending Name;" + spendingType.Id });
            db.SaveChanges();

            return View(spendingType);
        }

        // GET: SpendingTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpendingType spendingType = db.SpendingTypes.Find(id);
            if (spendingType == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "SpendingTypes;Delete;Get;" + ";Spending Type Id;" + spendingType.Id + ";Spending Name;" + spendingType.Id });
            db.SaveChanges();

            return View(spendingType);
        }

        // POST: SpendingTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SpendingType spendingType = db.SpendingTypes.Find(id);
            db.SpendingTypes.Remove(spendingType);
            db.SaveChanges();

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "SpendingTypes;Delete;Post;" + ";Spending Type Id;" + spendingType.Id + ";Spending Name;" + spendingType.Id });
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
