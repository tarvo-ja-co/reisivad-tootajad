﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project_ReisivadTöötajad.Models;

namespace Project_ReisivadTöötajad.Controllers
{
    public class HomeController : Controller
    {
        TagaridaEntities db = new TagaridaEntities();

        public HomeController()
        {
            ViewBag.Controller = "Home";
        }

        public ActionResult Index()
        {
            ViewBag.Kasutaja =
                 Request.IsAuthenticated
                 ?
                     ViewBag.Kasutaja = Person.GetByEmail(User.Identity.Name)
                     ?.FullName ?? "Võõras kasutaja"
                 :
                     "";

            ViewBag.Rollid = 
                Request.IsAuthenticated 
                ?
                string.Join(",", Person.GetByEmail(User.Identity.Name)?.UserInRoles.Select(x => " " + x.Role.RoleName)?? (new string[] { }))
                : 
                "";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

            public ActionResult Contact()  //Admin
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}