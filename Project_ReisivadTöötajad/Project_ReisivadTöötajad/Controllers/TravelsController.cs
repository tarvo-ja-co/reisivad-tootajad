﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_ReisivadTöötajad.Models;
using PagedList;


namespace Project_ReisivadTöötajad.Models
{

    partial class Travel
    {
        public static Dictionary<int, string> ReportStates = new Dictionary<int, string>
        {
            {1, "Created"},
            {2, "Sent For Approval"},
            {3, "Declined"},
            {4, "Approved"},
            {5, "Paid Out" }

        };

        public string _ReportState =>
            ReportStates.ContainsKey(ReportState) ? ReportStates[this.ReportState] : "";


        public int Duration => StartDate.HasValue && EndDate.HasValue ? ((EndDate.Value - StartDate.Value).Days) + 1 : 0;
        public int NightsDuration => StartDate.HasValue && EndDate.HasValue ? (EndDate.Value - StartDate.Value).Days : 0;


        int DaysOver => Duration - 15;
        public int DailyAllowance => Duration > 15 ? (15 * 50) + (DaysOver * 32) : Duration * 50;  // see arvutus on ka _Layout scriptis, mida kasutatakse spendingu create ja edit view'des

    }
}

namespace Project_ReisivadTöötajad.Controllers
{
    public class TravelsController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        public TravelsController()
        {
            ViewBag.Controller = "Travels";
        }

        public ActionResult Index2(string sortOrder, string searchString, int? page, int? personId, int? currentPersonId, int? statusId, int? currentStatusId, String fromDate, DateTime? currentFromDate, String toDate, DateTime? currentToDate)   //lisatud sorteerimise ja otsingu funktsionaalsus
        => Index(sortOrder, searchString, page, personId, currentPersonId, statusId, currentStatusId, fromDate, currentFromDate, toDate, currentToDate);   //lisatud sorteerimise ja otsingu funktsionaalsus


        // GET: Travels
        public ActionResult Index(string sortOrder, string searchString, int? page, int? personId, int? currentPersonId, int? statusId, int? currentStatusId, String fromDate, DateTime? currentFromDate, String toDate, DateTime? currentToDate)   //lisatud sorteerimise ja otsingu funktsionaalsus
        // public ViewResult Index(string sortOrder, string searchString)
        {
            var loginUser = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();  //sisseloginu andmed

            if (loginUser == null || loginUser.PersonState == 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Access Denied");
            }

            else if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Admin")) //Admin  näeb kõigi nimesid listis
            {
                ViewBag.PersonId = new SelectList(db.People.OrderBy(x => x.LastName).OrderBy(x => x.FirstName), "Id", "FullName");
            }
            else if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Accountant")) // Raamatupidaja näeb kõigi nimesid listis
            {
                ViewBag.PersonId = new SelectList(db.People.OrderBy(x => x.LastName).OrderBy(x => x.FirstName), "Id", "FullName");
            }
            else if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Manager"))  //Juht näeb oma inimesi listis
            {
                ViewBag.PersonId = new SelectList(db.People
                    .OrderBy(x => x.LastName).OrderBy(x => x.FirstName)
                    .Where(x => x.DepartmentID == loginUser.DepartmentID)
                    .ToList(), "Id", "FullName");
            }
            else
            {
                ViewBag.PersonId = new SelectList(db.People     //Teised näevad vaid enda nime
                    .OrderBy(x => x.LastName).OrderBy(x => x.FirstName)
                    .Where(t => t.Email == User.Identity.Name)
                    .ToList(), "Id", "FullName");  
            }
            
            ViewBag.CurrentSort = sortOrder;
            ViewBag.ReportState = Travel.ReportStates; //Sellega kuvatakse nimelised väärtused
            ViewBag.statusId = new SelectList(Travel.ReportStates, "Key", "Value");
            ViewBag.IconAfterText = " |";
            ViewBag.IconBeforeText = "| ";
            
            DateTime? fDate = DateTime.TryParse(fromDate, out DateTime fd) ? fd : DateTime.Now.AddMonths(-6);
            //DateTime cFromDate = DateTime.TryParse(currentFromDate, out DateTime cfd) ? cfd : DateTime.MinValue;
            DateTime? tDate = DateTime.TryParse(toDate, out DateTime td) ? td : DateTime.Now;
            //DateTime cToDate = DateTime.TryParse(currentToDate, out DateTime ctd) ? ctd : DateTime.MaxValue;
                       
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";

            if (personId > 0)
            { page = 1;}
            else
            { personId = currentPersonId; }
            ViewBag.currentPersonId = personId;

            if (statusId > 0)
            { page = 1; }
            else
            { statusId = currentStatusId; }
            ViewBag.currentStatusId = statusId;

            if (!String.IsNullOrEmpty(fromDate))
            { page = 1; }
            else
            {
                if (page > 0)
                { fDate = currentFromDate; }
            } 
            ViewBag.currentFromDate = fDate;

            ViewBag.fromDate = String.IsNullOrEmpty(fromDate) ? DateTime.Now.AddMonths(-6).AddDays(1).ToString("dd.MM.yyyy") : fromDate;
            if (page > 0)
            {
                ViewBag.fromDate = fDate?.ToString("dd.MM.yyyy");
            }

            if (!String.IsNullOrEmpty(toDate))
            { page = 1; }
            else
            {
                if (page > 0)
                { tDate = currentToDate; }
            }
            ViewBag.currentToDate = tDate;

            ViewBag.toDate = String.IsNullOrEmpty(toDate) ? DateTime.Now.ToString("dd.MM.yyyy") : toDate;
            if (page > 0)
            {
                ViewBag.toDate = tDate?.ToString("dd.MM.yyyy");
            }

            var travels = from s in db.Travels
                          .Include(t => t.Person)
                          select s;

            //Töötajate listi nähtavuse defineerimine vastavalt sisseloginu rollile

            if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Admin"))  //Admin näeb kõiki
            {
                travels = travels.Where(v => v.StartDate >= fDate && v.StartDate <= tDate);
            }
            else if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Accountant"))  //Raamatupidaja näeb kõiki
            {
                travels = travels.Where(v => v.StartDate >= fDate && v.StartDate <= tDate);
            }
            else if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Manager"))  //Juht näeb oma inimesi listis
            {
                travels = travels.Where(v => v.StartDate >= fDate && v.StartDate <= tDate && v.Person.DepartmentID == loginUser.DepartmentID);
            }
            else
            {
                travels = travels.Where(v => v.StartDate >= fDate && v.StartDate <= tDate && v.Person.Email == User.Identity.Name); //Teised näevad vaid enda nime
            }


            if (personId > 0)
            { travels = travels.Where(v => v.PersonId == personId); }

            if (statusId > 0)
            { travels = travels.Where(v => v.ReportState == statusId); }
            //if (!String.IsNullOrEmpty(searchString))
            //{ travels = travels.Where(s => s.TravelDestination.Contains(searchString) || s.TripPurpose.Contains(searchString));}

            switch (sortOrder)
            {
                case "name_desc":
                    travels = travels.OrderByDescending(s => s.Person.FirstName);
                    break;
                case "Date":
                    travels = travels.OrderBy(s => s.StartDate);
                    break;
                case "date_desc":
                    travels = travels.OrderByDescending(s => s.StartDate);
                    break;
                default:
                    //travels = travels.OrderBy(s => s.Person.FirstName);
                    travels = travels.OrderByDescending(s => s.StartDate);
                    break;
            }
            int pageSize = 20; //siin saab muuta, mitu kirjet ühel lehel kuvatakse.
            int pageNumber = (page ?? 1);

           /*ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Travels;Details;Get;" + ";Travel Id;" + "" + ";Travel Name;" + "" + ";Travel Destination;" + "" + ";Trip Purpose;" + ";Start Date;" + "" + ";End Date;" + "" + ";Comments;"  + "" + ";Report Status;" + "" });
            db.SaveChanges();*/ //Võtsin praegu välja

            return View(travels.ToPagedList(pageNumber, pageSize));
            //return View(travels.Skip(page * size).Take(size).ToList());
            //return View(travels.ToList());
        }

        // GET: Travels/Details/5
        public ActionResult Details(int? id)
        {
            var loginUser = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();  //sisseloginu andmed
            ViewBag.ReportState = Travel.ReportStates; //Sellega kuvatakse nimelised väärtused
            ViewBag.Icon = " |";

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }

           /* ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Travels;Details;Get;" + ";Travel Id;" + travel.Id + ";Travel Destination;" + travel.TravelDestination + ";Trip Purpose;" + travel.TripPurpose + ";Start Date;" + travel.StartDate + ";End Date;" + travel.EndDate + ";Comments;" + travel.Description + ";Report Status;" + travel.ReportState });
            db.SaveChanges();*/ //võtan praegu välja

            return View(travel);
        }

        // GET: Travels/Create
        public ActionResult Create()
        {           
            var loginUser = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();  //sisseloginu andmed

            //Töötajate listi nähtavuse defineerimine vastavalt sisseloginu rollile
            if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Admin")) //Admin näeb kõigi nimesid listis
            {
                ViewBag.PersonId = new SelectList(db.People.OrderBy(x => x.LastName).OrderBy(x => x.FirstName).Where(x => x.PersonState == 0), "Id", "FullName");
            }
            //else if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Juht"))  //Juht näeb oma inimesi listis
            //{ 
            //    ViewBag.PersonId = new SelectList(db.People
            //        .OrderBy(x => x.LastName).OrderBy(x => x.FirstName)
            //        .Where(t => t.DepartmentID == loginUser.DepartmentID)
            //        .ToList(), "Id", "FullName");
            //}
            else
            {
                ViewBag.PersonId = new SelectList(db.People
                    .OrderBy(x => x.LastName).OrderBy(x => x.FirstName)
                    .Where(x => x.PersonState == 0)
                    .Where(t => t.Email == User.Identity.Name)
                    .ToList(), "Id", "FullName");  //Teised näevad vaid enda nime
            }

            //Staatuste listi nähtavuse defineerimine vastavalt sisseloginu rollile

            Dictionary<int, string> ReportStates = new Dictionary<int, string>();

            if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Employee"))  //Töötaja näeb staatuseid 1 ja 2
            {
                    ReportStates.Add(1, "Created");
            }
                    ////    ReportState = ReportState.Where(t => t.Key == 3 || t.Key == 4 || t.Key == 5).ToList();
                    ////}

                    ////ViewBag.ReportState = new SelectList(Travel.ReportStates
                    ////     .Where(t => t.Key == 5)
                    ////     .ToList(), "Key", "Value");
             ViewBag.ReportState = new SelectList(ReportStates, "Key", "Value");

            return View();
        }


        // POST: Travels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create([Bind(Include = "Id,StartDate,EndDate,Description,ReportState,PersonId,TravelDestination,TripPurpose,TravelName")] Travel travel)

        {
            if (ModelState.IsValid)
            {
                db.Travels.Add(travel);
                db.SaveChanges();

                ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
                db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Travels;Create;Post;" + ";Travel Id;" + travel.Id + ";Travel Destination;" + travel.TravelDestination + ";Trip Purpose;" + travel.TripPurpose + ";Start Date;" + travel.StartDate + ";End Date;" + travel.EndDate + ";Comments;" + travel.Description + ";Report Status;" + travel.ReportState });
                db.SaveChanges();


                TempData["Success"] = "Saved Successfully!";
                return RedirectToAction("Edit", new { id = travel.Id });
            }

            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName", travel.PersonId);
            ViewBag.ReportState = new SelectList(Travel.ReportStates, "Key", "Value");

            return View(travel);
            
        }

        // GET: Travels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }

            //Töötajate listi nähtavuse defineerimine vastavalt sisseloginu rollile
            var loginUser = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();  //sisseloginu andmed

            if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Admin")) //Admin näeb kõigi nimesid listis
            {
                ViewBag.PersonId = new SelectList(db.People.OrderBy(x => x.LastName).OrderBy(x => x.FirstName).Where(x => x.PersonState == 0), "Id", "FullName", travel.PersonId);
            }
            else if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Accountant")) //Raamatupidaja näeb kõigi nimesid listis
            {
                ViewBag.PersonId = new SelectList(db.People.OrderBy(x => x.LastName).OrderBy(x => x.FirstName).Where(x => x.PersonState == 0), "Id", "FullName", travel.PersonId);
            }
            else if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Manager"))  //Juht näeb oma inimesi listis
            {
                ViewBag.PersonId = new SelectList(db.People
                    .OrderBy(x => x.LastName).OrderBy(x => x.FirstName)
                    .Where(x => x.PersonState == 0)
                    .Where(t => t.DepartmentID == loginUser.DepartmentID)
                    .ToList(), "Id", "FullName", travel.PersonId);
            }
            else
            {
                ViewBag.PersonId = new SelectList(db.People
                    .OrderBy(x => x.LastName).OrderBy(x => x.FirstName)
                    .Where(x => x.PersonState == 0)
                    .Where(t => t.Email == User.Identity.Name)
                    .ToList(), "Id", "FullName", travel.PersonId);  //Teised näevad vaid enda nime
            }


            //Staatuste listi nähtavuse defineerimine vastavalt sisseloginu rollile

            Dictionary<int, string> ReportStates = new Dictionary<int, string>();

            if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Employee"))  //Töötaja näeb staatuseid 1 ja 2
            {
                ReportStates.Add(1, "Created");
                ReportStates.Add(2, "Sent For Approval");
            }
            if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Manager"))  //Juht näeb 1-4 staatust
            {
                ReportStates.Add(3, "Declined");
                ReportStates.Add(4, "Approved");
            }

            if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Accountant"))  //Juht näeb oma inimesi listis
            {
                ReportStates.Add(5, "Paid Out");
            }
            if (loginUser.UserInRoles.Select(x => x.Role.RoleName).Contains("Admin")) //Admin näeb kõiki staatusi
            {
                if (!ReportStates.ContainsKey(1)) { ReportStates.Add(1, "Created"); }
                if (!ReportStates.ContainsKey(2)) { ReportStates.Add(2, "Sent For Approval"); }
                if (!ReportStates.ContainsKey(3)) { ReportStates.Add(3, "Declined"); }
                if (!ReportStates.ContainsKey(4)) { ReportStates.Add(4, "Approved"); }
                if (!ReportStates.ContainsKey(5)) { ReportStates.Add(5, "Paid Out"); }
            }
            ////    ReportState = ReportState.Where(t => t.Key == 3 || t.Key == 4 || t.Key == 5).ToList();
            ////}

            ////ViewBag.ReportState = new SelectList(Travel.ReportStates
            ////     .Where(t => t.Key == 5)
            ////     .ToList(), "Key", "Value");
            ViewBag.ReportState = new SelectList(ReportStates, "Key", "Value");
            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Travels;Edit;Get;" + ";Travel Id;" + travel.Id + ";Travel Destination;" + travel.TravelDestination + ";Trip Purpose;" + travel.TripPurpose + ";Start Date;" + travel.StartDate + ";End Date;" + travel.EndDate + ";Comments;" + travel.Description + ";Report Status;" + travel.ReportState });
            db.SaveChanges();


            // ViewBag.PersonId = new SelectList(db.People, "Id", "PersonID", travel.PersonId);   SELLE VÕIB ILMSELT ÄRA KUSTUTADA - SEE LOOB ISIKUKOODIDE LISTI
            return View(travel);
        }

        // POST: Travels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id, PersonId, StartDate, EndDate, Description, ReportState, TravelDestination, TripPurpose, TravelName")] Travel travel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(travel).State = EntityState.Modified;
                db.SaveChanges();
                //return RedirectToAction("Index");
            }
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName", travel.PersonId);
            ViewBag.ReportState = new SelectList(Travel.ReportStates, "Key", "Value");

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Travels;Edit;Post;" + ";Travel Id;" + travel.Id + ";Travel Destination;" + travel.TravelDestination + ";Trip Purpose;" + travel.TripPurpose + ";Start Date;" + travel.StartDate + ";End Date;" + travel.EndDate + ";Comments;" + travel.Description + ";Report Status;" + travel.ReportState });
            db.SaveChanges();


            TempData["Success"] = "Saved Successfully!";
            return View(travel);
        }

        // GET: Travels/Delete/5
        public ActionResult Delete(int? id)
        {
            ViewBag.ReportState = Travel.ReportStates; //Sellega kuvatakse nimelised väärtused
           // ViewBag.ReportState = new SelectList(Travel.ReportStates, "Key", "Value"); //Kustutasin ära kuna näitab väärtust arvudes

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Travel travel = db.Travels.Find(id);
            if (travel == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Travels;Delete;Get;" + ";Travel Id;" + travel.Id  + ";Travel Destination;" + travel.TravelDestination + ";Trip Purpose;" + travel.TripPurpose + ";Start Date;" + travel.StartDate + ";End Date;" + travel.EndDate + ";Comments;" + travel.Description + ";Report Status;" + travel.ReportState });
            db.SaveChanges();

            return View(travel);
        }

        // POST: Travels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id) //, int travelid)
        {

            //var spendingfile = db.SpendingFiles.Where(x => x.SpendingId == db.Spendings.id).FirstOrDefault();
            //if (spendingfile != null)
            //{
            //    db.SpendingFiles.Remove(spendingfile);
            //    db.SaveChanges();
            //}

            //var spendingFiles = db.SpendingFiles
            //        .Include(s => s.Spending)
            //        .Where(t => t.SpendingId == travelid);


//            DELETE C, SC, I
//FROM categories C
//INNER JOIN sub_categories SC ON C.id = SC.category_id
//LEFT JOIN items I ON SC.id = I.subcategory_id
//WHERE C.id = 5;


           // db.SpendingFiles
           //     .Include(p => p.Spending)
           //     .Where(p => p.SpendingId == travelid)
           //.ToList()
           //.ForEach(p => db.SpendingFiles.Remove(p));
           // db.SaveChanges();

            //db.Spendings.Where(p => p.TravelId == id)
            //   .ToList()
            //   .ForEach(p => db.Spendings.Remove(p));
            //db.SaveChanges();

            Travel travel = db.Travels.Find(id);
            foreach(var s in travel.Spendings.ToList())
            {

                foreach (var f in s.SpendingFiles.ToList())
                {
                    File file = f.File;
                    db.SpendingFiles.Remove(f);
                    //db.SaveChanges();

                    db.Files.Remove(file);
                    db.SaveChanges();

                }

                db.Spendings.Remove(s);
                db.SaveChanges();
                
            }
            db.Travels.Remove(travel);
            db.SaveChanges();

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Travels;Delete;Post;" + ";Travel Id;" + travel.Id + ";Travel Destination;" + travel.TravelDestination + ";Trip Purpose;" + travel.TripPurpose + ";Start Date;" + travel.StartDate + ";End Date;" + travel.EndDate + ";Comments;" + travel.Description + ";Report Status;" + travel.ReportState });
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
