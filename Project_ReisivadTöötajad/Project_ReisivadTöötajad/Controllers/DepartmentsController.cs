﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_ReisivadTöötajad.Models;

namespace Project_ReisivadTöötajad.Controllers
{
    public class DepartmentsController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        // GET: Departments
        public ActionResult Index()
        {
            var loginUser = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();  //sisseloginu andmed

            if (loginUser == null || loginUser.PersonState == 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Access Denied");
            }

            var departments = db.Departments.Include(d => d.File);


            /*ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Departments;Index;Get;" + ";Department Id;" + "" + ";Department Name;" + "" });
            db.SaveChanges();*/ //võtsin praegu välja

            return View(departments.ToList());
        }

        // GET: Departments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Departments;Details;Get;" + ";Department Id;" + department.Id + ";Department Name;" + department.DepartmentName });
            db.SaveChanges();

            return View(department);
        }

        // GET: Departments/Create
        public ActionResult Create()
        {
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename");
            return View();
        }

        // POST: Departments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DepartmentName,Picture")] Department department)
        {
            if (ModelState.IsValid)
            {
                db.Departments.Add(department);
                db.SaveChanges();

                ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
                db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Departments;Create;Post;" + ";Department Id;" + department.Id + ";Department Name;" + department.DepartmentName });
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", department.Picture);
            return View(department);
        }

        // GET: Departments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", department.Picture);

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Departments;Edit;Get;" + ";Department Id;" + department.Id + ";Department Name;" + department.DepartmentName });
            db.SaveChanges();

            return View(department);
        }

        // POST: Departments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DepartmentName,Picture")] Department department)
        {
            if (ModelState.IsValid)
            {
                db.Entry(department).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", department.Picture);

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;

            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Departments;Edit;Post;" + ";Department Id;" + department.Id + ";Department Name;" + department.DepartmentName });
            db.SaveChanges();

            return View(department);
        }

        // GET: Departments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }

           /* ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Departments;Delete;Get;" + ";Department Id;" + department.Id + ";Department Name;" + department.DepartmentName });
            db.SaveChanges();*/ //võtsin praegu välja

            return View(department);
        }

        // POST: Departments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Department department = db.Departments.Find(id);
            db.Departments.Remove(department);
            db.SaveChanges();

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Departments;Delete;Post;" + ";Department Id; " + department.Id + ";Department Name; " + department.DepartmentName });
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
