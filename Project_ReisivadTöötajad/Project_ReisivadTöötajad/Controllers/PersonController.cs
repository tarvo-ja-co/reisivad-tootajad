﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_ReisivadTöötajad.Models;

namespace Project_ReisivadTöötajad.Models
{
    partial class Person
    {

        public static Dictionary<int, string> PersonStates = new Dictionary<int, string>
        {
            {0, "Currently Employed"},
            {1, "Has Left the Company"},

        };
        public string FullName => FirstName + " " + LastName;

        public static Person GetByEmail(string email)
        {
            //static 
                TagaridaEntities db = new TagaridaEntities();
            return db.People.Where(x => x.Email == email).SingleOrDefault();
        }

        public bool IsInRole(string rolename)
            => this.UserInRoles
                .Select(x => x.Role.RoleName.ToLower()).ToList()
                .Intersect(rolename.Split(',').Select(y => y.Trim().ToLower()))
                .Count() > 0;
    }
}

namespace Project_ReisivadTöötajad.Controllers  
{
    public class PersonController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        // GET: Person
        public ActionResult Index(int? personId, int? departmentId, int? stateId)
        {
            var loginUser = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();  //sisseloginu andmed

            if (loginUser == null || loginUser.PersonState == 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Access Denied");
            }

            var people = db.People
                .Include(p => p.Department)
                .Include(p => p.File)
                .OrderBy(x => x.LastName).OrderBy(x => x.FirstName)
                .ToList();

            ViewBag.personId = new SelectList(db.People.OrderBy(x => x.LastName).OrderBy(x => x.FirstName), "Id", "FullName");
            ViewBag.departmentId = new SelectList(db.Departments.OrderBy(x => x.DepartmentName), "Id", "DepartmentName");
            ViewBag.PersonState = Person.PersonStates; //Sellega kuvatakse nimelised väärtused
            ViewBag.stateId = new SelectList(Person.PersonStates, "Key", "Value");

            /* ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
             db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Person;Index;Get;" + ";Person Id;" + "" + ";Full Name;" + "" + ";Department ID;" + "" + ";State;" + "" + ";Start Date;" + "" });
             db.SaveChanges(); */ //võtsin praegu välja                                                                            

            if (personId > 0)
            {
                people = people.Where(v => v.Id == personId).ToList();
            }

            if (departmentId > 0)
            {
                people = people.Where(v => v.Department.Id == departmentId).ToList();
            }

            if (stateId >= 0)
            {
                people = people.Where(v => v.PersonState == stateId).ToList();
            }

            return View(people);
        }

        public ActionResult AddRole(int? id, int? roleid)           // parameetritena defineeritakse Person tabeli id (töötaja id) ja Roles tabeli id (rolli id)
        {
            if (id == null)             // kontrollib, kui parameetiks olev id on tühi, siis annab veateate
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);             // defineerib Person tüüpi muutuja, millele omistatakse Person tabeli väljad, mis on seotud etteantud töötaja id-ga (id)
            if (person == null)                             // kui id vastet ei leia, siis annab veateate
            {
                return HttpNotFound();
            }

            if (roleid == null)         // kontrollib, kui parameetiks olev roleid on tühi, siis annab veateate
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Access Denied");
            }
            Role role = db.Roles.Find(roleid);              // defineerib Role tüüpi muutuja, millele omistatakse Role tabeli väljad, mis on seotud etteantud rolli id-ga (roleid)
            if (role == null)                               // kui roleid vastet ei leia, siis annab veateate
            {
                return HttpNotFound();
            }

            try
            {
                db.UserInRoles.Add(new UserInRole { PersonId = id.Value, RoleId = roleid.Value });   // Tabelisse UserInRoles lisatakse uus kirje
                db.SaveChanges();
            }
            catch { }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Person;AddRole;;" + ";Person Id;" + person.Id + ";Full Name;" + person.FullName + ";Department ID;" + person.DepartmentID + ";State;" + person.PersonState + ";Start Date;" + person.StartDate });
            db.SaveChanges();

            return RedirectToAction("Edit", new { id = id });
        }

        public ActionResult RemoveRole(int? id, int? roleid)  // parameetritena defineeritakse Person tabeli id (töötaja id) ja Roles tabeli id (rolli id)
        {
            if (id == null)         // kontrollib, kui parameetiks olev id on tühi, siis annab veateate
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);      // defineerib Person tüüpi muutuja, millele omistatakse Person tabeli väljad, mis on seotud etteantud töötaja id-ga (id)
            if (person == null)                      // kui id vastet ei leia, siis annab veateate
            {
                return HttpNotFound();
            }

            if (roleid == null)         // kontrollib, kui parameetriks olev roleid on tühi, siis annab veateate
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var userinrole = person.UserInRoles.Where(x => x.RoleId == roleid).FirstOrDefault();        // defineeritakse muutuja, mis tuletatakse töötaja id-st ja rolli id-st, mis on valitud
            if (userinrole != null)
            {
                db.UserInRoles.Remove(userinrole);          // userinrole kustutatakse UserInRole tabelist
                db.SaveChanges();
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Person;RemoveRole;;" + ";Person Id;" + person.Id + ";Full Name;" + person.FullName + ";Department ID;" + person.DepartmentID + ";State;" + person.PersonState + ";Start Date;" + person.StartDate });
            db.SaveChanges();

            return RedirectToAction("Edit", new { id = id });
        }

        // GET: Person/Details/5

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            ViewBag.OtherRoles = db
                .Roles
                .Where(x => !x.UserInRoles.Select(y => y.PersonId).Contains(id.Value)).ToList();

            /*ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Person;Details;Get;" + ";Person Id;" + person.Id + ";Full Name;" + person.FullName + ";Department ID;" + person.DepartmentID + ";State;" + person.PersonState + ";Start Date;" + person.StartDate });
            db.SaveChanges();*/ //Võtsin praegu välja

            return View(person);
        }


        // GET: Person/Create
        public ActionResult Create()
        {
            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName");
            //ViewBag.Picture = new SelectList(db.Files, "Id", "Filename");

            return View();
        }

        // POST: Person/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,DepartmentID,Email")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();

                ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
                db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Person;Create;Post;" + ";Person Id;" + person.Id + ";Full Name;" + person.FullName + ";Department ID;" + person.DepartmentID + ";State;" + person.PersonState + ";Start Date;" + person.StartDate });
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName", person.DepartmentID);
            //ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", person.Picture);

            return View(person);
        }

        // GET: Person/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound(); 
            }
            ViewBag.OtherRoles = db
               .Roles
               .Where(x => !x.UserInRoles.Select(y => y.PersonId).Contains(id.Value)).ToList();
            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName", person.DepartmentID);
            ViewBag.PersonState = new SelectList(Person.PersonStates, "Key", "Value");
            //ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", person.Picture);


            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Person;Edit;Get;" + ";Person Id;" + person.Id + ";Full Name;" + person.FullName + ";Department ID;" + person.DepartmentID + ";Sate;" + person.PersonState + ";StartDate;" + person.StartDate });
            db.SaveChanges();

            //modelBuilder.Entity<Items>()
            //    .Property(i => i.Id)
            //    .ForSqlServer()
            //    .UseIdentity()

            return View(person);
        }

        // POST: Person/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,PersonState,DepartmentID,Email")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();

                ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
                db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Person;Edit;Post;" + ";Person Id;" + person.Id + ";Full Name;" + person.FullName + ";Department ID;" + person.DepartmentID + ";State;" + person.PersonState + ";Start Date;" + person.StartDate });
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.DepartmentID = new SelectList(db.Departments, "Id", "DepartmentName", person.DepartmentID);
            //ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", person.Picture);

            return View(person);
        }

        // GET: Person/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Person;Delete;Get;" + ";Person Id;" + person.Id + ";Full Name;" + person.FullName + ";Department ID;" + person.DepartmentID + ";State;" + person.PersonState + ";Start Date;" + person.StartDate });
            db.SaveChanges();

            return View(person);
        }

        // POST: Person/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Person;Delete;Post;" + ";Person Id;" + person.Id + ";Full Name;" + person.FullName + ";Department ID;" + person.DepartmentID + ";State;" + person.PersonState + ";Start Date;" + person.StartDate });
            db.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
