﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_ReisivadTöötajad.Models;


namespace Project_ReisivadTöötajad.Controllers
{
    public class SpendingsController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        // GET: Spendings
        public ActionResult Index(int? travelid)
        {
            var loginUser = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();  //sisseloginu andmed

            if (loginUser == null || loginUser.PersonState == 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Access Denied");
            }


            var spendings = db.Spendings
                .Include(s => s.File)
                .Include(s => s.Travel)
                .Include(s => s.SpendingType)
                .Where(t=> t.TravelId == travelid)
                ;

            if (travelid == null)         // kontrollib, kui parameetiks olev travelid on tühi, siis annab veateate
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            //            Spending spending = db.Spendings.Find(travelid);      // defineerib Spending tüüpi muutuja, millele omistatakse Spending tabeli väljad, mis on seotud etteantud kulurea id-ga (id)


            ViewBag.TravelId = travelid;     // jätab travelid meelde, et hiljem saaks uut kulukirjet lisada ja sellele aktiivne travelid omistada
            ViewBag.TotalAmount = db.Spendings
                .Where(t => t.TravelId == travelid)
                .Sum(x => x.Amount);

            ViewBag.IconAfterText = " |";
            ViewBag.IconBeforeText = "| ";


            return View(spendings.ToList());
        }


        public ActionResult RemoveFile(int? spendingid, int? fileid)  // parameetritena defineeritakse Spending tabeli id (kulurea id) ja File tabeli id (file id)
        {
            if (spendingid == null)         // kontrollib, kui parameetiks olev spendingid on tühi, siis annab veateate
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(spendingid);      // defineerib Spending tüüpi muutuja, millele omistatakse Spending tabeli väljad, mis on seotud etteantud kulurea id-ga (id)
            if (spending == null)                      // kui id vastet ei leia, siis annab veateate
            {
                return HttpNotFound();
            }

            if (fileid == null)         // kontrollib, kui parameetriks olev fileid on tühi, siis annab veateate
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var spendingfile = spending.SpendingFiles.Where(x => x.FileId == fileid).FirstOrDefault();        // defineeritakse muutuja, mis tuletatakse spending id-st ja file id-st, mis on valitud
            if (spendingfile != null)
            {
                db.SpendingFiles.Remove(spendingfile);          // spendingfile (kombinatsioon spending id ja file id) kustutatakse SpendingFiles tabelist
                db.SaveChanges();
            }

            File deletefile = db.Files.Find(fileid);      //fileid järgi otsitakse File tabelist vastav rida ja kustutatakse see
            db.Files.Remove(deletefile);

            return RedirectToAction("Edit", new { id = spendingid });   
        }



        // GET: Spendings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }

            /*ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Spendings;Details;Get;" + ";Spending Id;" + spending.Id + ";Start Date;" + spending.StartDate + ";End Date;" + spending.EndDate + "Amount" + spending.Amount + "VAT" + spending.VatEUR });
            db.SaveChanges();*/ //Võtan praegu välja

            ViewBag.Icon = " |";

            return View(spending);
        }

        
        // GET: Spendings/Create
        public ActionResult Create(int travelid )
        {
            //if (spendingid == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            Spending spending = new Spending { TravelId = travelid };

            ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName");
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
            return View(spending);
        }

        // POST: Spendings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TravelId,TypeId,Amount,ServiceProvider,StartDate,EndDate,VatEUR")] Spending spending)
        {
            if (ModelState.IsValid)
            {
                db.Spendings.Add(spending);
                db.SaveChanges();

                TempData["Success"] = "Saved Successfully!";
                return RedirectToAction("Edit", "Spendings", new { id = spending.Id });
               // return RedirectToAction("Edit", new { id = spending.Id });
                //return RedirectToAction("Edit", new { id = spendingid });
            }

            //ViewBag.Document = new SelectList(db.Files, "Id", "Filename", spending.Document);
            ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName", spending.TravelId);
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Spendings;Create;Post;" + ";Spending Id;" + spending.Id + ";Start Date;" + spending.StartDate + ";End Date;" + spending.EndDate + "Amount" + spending.Amount + "VAT" + spending.VatEUR });
            db.SaveChanges();
            
            return View(spending);
        }

        // GET: Spendings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }
            //ViewBag.Document = new SelectList(db.Files, "Id", "Filename", spending.Document);
            //ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName", spending.TravelId);
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);

            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Spendings;Edit;Get;" + ";Spending Id;" + spending.Id + ";Start Date;" + spending.StartDate + ";End Date;" + spending.EndDate + "Amount" + spending.Amount + "VAT" + spending.VatEUR });
            db.SaveChanges();
            
            return View(spending);
        }

        // POST: Spendings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit([Bind(Include = "Id,TravelId,TypeId,Amount,ServiceProvider,StartDate,EndDate,VatEUR")] Spending spending, HttpPostedFileBase file) // lisatud , HttpPostedFileBase file
        {
            if (ModelState.IsValid)
            {
                db.Entry(spending).State = EntityState.Modified;
                
           //     db.Entry(spending).Property(x => x.Document).IsModified = false;   //lisasime
                db.SaveChanges();

                //lisatud
                if (file != null && file.ContentLength > 0)
                {
                    using (System.IO.BinaryReader br = new System.IO.BinaryReader(file.InputStream))
                    {
                        //// ma lihtsuse mõttes kustutan vana pildi baasist
                        //File v = db.Files.Find(spending.Document ?? 0);
                        
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        File f = new File { Content = buff, ContentType = file.ContentType, Filename = file.FileName.Split('\\').Last().Split('/').Last() };
                        db.Files.Add(f);
                        db.SaveChanges();
                        spending.Document = f.Id;
                        db.SaveChanges();

                        db.SpendingFiles.Add(new SpendingFile { SpendingId = spending.Id, FileId = f.Id });   // Tabelisse SpendingFiles lisatakse uus kirje
                        db.SaveChanges();

                        //if (v != null)
                        //{
                        //    db.Files.Remove(v);
                        //    db.SaveChanges();
                        //}
                    }
                } //lisatud
               
                ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
                db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Spendings;Delete;Post;" + ";Spending Id;" + spending.Id + ";Start Date;" + spending.StartDate + ";End Date;" + spending.EndDate + "Amount" + spending.Amount + "VAT" + spending.VatEUR });
                db.SaveChanges();

     //           return RedirectToAction("Edit");

                TempData["Success"] = "Saved Successfully!";
                return RedirectToAction("Edit");
            }
            //ViewBag.Document = new SelectList(db.Files, "Id", "Filename", spending.Document);
            //ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName", spending.TravelId);
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);

            return View(spending);


        }

       
        [HttpPost]
        public JsonResult GetStartDate(int TravelID)
        {
            var query = db.Travels.Where(c => c.Id == TravelID).FirstOrDefault();
            return Json(query.StartDate?.ToString("dd/MM/yyyy"), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetEndDate(int TravelID)
        {
            var query = db.Travels.Where(c => c.Id == TravelID).FirstOrDefault();
            return Json(query.EndDate?.ToString("dd/MM/yyyy"), JsonRequestBehavior.AllowGet);
        }

        //Päevarahade arvutus automaatselt, kui Spendingu tüüp = Päevaraha
        [HttpPost]
        public JsonResult GetDailyAllowance(int TravelID)
        {
            var query = db.Travels.Where(c => c.Id == TravelID).FirstOrDefault();

                int Duration = query.StartDate.HasValue && query.EndDate.HasValue ? ((query.EndDate.Value - query.StartDate.Value).Days) + 1 : 0;
                int DaysOver = Duration - 15;
                int DailyAllowance = Duration > 15 ? (15 * 50) + (DaysOver * 32) : Duration * 50;
             
            return Json(DailyAllowance, JsonRequestBehavior.AllowGet);
        }


    // GET: Spendings/Delete/5
    public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }
  
            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Spendings;Delete;Get;" + ";Spending Id;" + spending.Id + ";Start Date;" + spending.StartDate + ";End Date;" + spending.EndDate + "Amount" + spending.Amount + "VAT" + spending.VatEUR });
            db.SaveChanges();
            
            return View(spending);
        }

        // POST: Spendings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            
            var spendingfile = db.SpendingFiles.Where(x => x.SpendingId == id).FirstOrDefault();       
            if (spendingfile != null)
            {
                db.SpendingFiles.Remove(spendingfile);         
                db.SaveChanges();
            }

            Spending spending = db.Spendings.Find(id);
            db.Spendings.Remove(spending);
            db.SaveChanges();
          
            ViewBag.UserId = Person.GetByEmail(User.Identity.Name).Id;
            db.ReportLogs.Add(new ReportLog { LogDate = DateTime.Now, UserID = ViewBag.UserId, LogRecord = "Spendings;Delete;Post;" + ";Spending Id;" + spending.Id + ";Start Date;" + spending.StartDate + ";End Date;" + spending.EndDate + "Amount" + spending.Amount + "VAT" + spending.VatEUR });
            db.SaveChanges();

            return RedirectToAction("Edit","Travels", new { id = spending.TravelId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
