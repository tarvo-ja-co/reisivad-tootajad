﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Project_ReisivadTöötajad.Models;

namespace Project_ReisivadTöötajad.Controllers
{
    public class ReportLogsController : Controller
    {
        private TagaridaEntities db = new TagaridaEntities();

        // GET: ReportLogs
        public ActionResult Index()
        {
            var loginUser = db.People.Where(x => x.Email == User.Identity.Name).SingleOrDefault();  //sisseloginu andmed

            if (loginUser == null ||loginUser.PersonState == 1)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Access Denied" +
                    "");
            }

            return View(db.ReportLogs.OrderByDescending(id => id.LogDate).ToList());
        }

        // GET: ReportLogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReportLog reportLog = db.ReportLogs.Find(id);
            if (reportLog == null)
            {
                return HttpNotFound();
            }
            return View(reportLog);
        }

        // GET: ReportLogs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ReportLogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,LogDate,UserID,LogRecord")] ReportLog reportLog)
        {
            if (ModelState.IsValid)
            {
                db.ReportLogs.Add(reportLog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(reportLog);
        }

        // GET: ReportLogs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReportLog reportLog = db.ReportLogs.Find(id);
            if (reportLog == null)
            {
                return HttpNotFound();
            }
            return View(reportLog);
        }

        // POST: ReportLogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,LogDate,UserID,LogRecord")] ReportLog reportLog)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reportLog).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(reportLog);
        }

        // GET: ReportLogs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReportLog reportLog = db.ReportLogs.Find(id);
            if (reportLog == null)
            {
                return HttpNotFound();
            }
            return View(reportLog);
        }

        // POST: ReportLogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ReportLog reportLog = db.ReportLogs.Find(id);
            db.ReportLogs.Remove(reportLog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
