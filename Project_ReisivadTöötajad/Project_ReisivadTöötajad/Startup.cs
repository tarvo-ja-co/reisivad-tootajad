﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Project_ReisivadTöötajad.Startup))]
namespace Project_ReisivadTöötajad
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
