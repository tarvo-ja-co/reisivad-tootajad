﻿using System.Web;
using System.Web.Mvc;

namespace Reisivad_töötajad
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
